/************************ Type Conversion and Coercion - Starts *****************/
let test_var = 6
console.log(typeof test_var);
test_var = String(test_var);
console.log(typeof test_var);
test_var = "6";
console.log(typeof test_var);
test_var = Number(test_var);
console.log(typeof test_var);
test_var = 6;
console.log(typeof test_var);
test_var = test_var + "";
console.log(test_var);
console.log(typeof test_var);
test_var = test_var - 2;
console.log(test_var);
console.log(typeof test_var);
test_var = 8;
test_var = Boolean(test_var);
console.log(test_var);
test_var = 0;
test_var = Boolean(test_var);
console.log(test_var);
console.log(Boolean(null));
console.log(Boolean(undefined));
test_var = "123String";
test_var = Number(test_var);
console.log(test_var);
console.log(typeof test_var);
test_var = "123String";
test_var = parseInt(test_var);
console.log(test_var);
test_var = "123String456";
test_var = parseInt(test_var);
console.log(test_var);
/************************ Type Conversion and Coercion - Starts *****************/


/************************ Arithmetic Operations - Starts *****************/
let num1 = 5;
let num2 = 47;
console.log(num1 + num2); // +, -, *, /, %
console.log(num1);
console.log(num1++);
console.log(num1);
console.log(++num1);
console.log(num1--);
console.log(--num1);
num1 = 4 * 4 * 4;
console.log(num1);
num1 = Math.pow(5, 3);
console.log(num1);
/************************ Arithmetic Operations - End *****************/


/************************ Relational Operatiors - Starts *****************/
let no1 = 8;
let no2 = 5;
console.log(no1 > no2); // <, >, <=, >=, ==, !
no1 = "5";
console.log(no1 == no2);
console.log(no1 === no2); // best to use
/************************ Relational Operators - End *****************/

// Reference: https://www.youtube.com/playlist?list=PLsyeobzWxl7rrvgG7MLNIMSTzVCDZZcT4