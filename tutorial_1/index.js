// This is my first js code.
console.log("Hello World");

let varName = "JavaScript";
console.log(varName);

/******************* variable conventions - Starts ******************/
// can't be reserve keywords
// should be meaning full
// can't start with number
// can't contain space or hypen(-)
// case sensitive 
/******************* variable conventions - Starts ******************/


/******************* primitive Types - Starts ***********************/
//1. string, 2. number, 3. boolean, 4. undefined, 5. null
let courseName = "JavaScript";
let year = 2023;
let isApproved = true;
let firstName;
let lastName = undefined;
let selectedCourse = null;
/******************* primitive Types - Ends ***********************/


/******************* dynamic typing - Starts ***********************/
let initialVal = "Tutorial";
console.log(typeof initialVal);
initialVal = 2;
console.log(typeof initialVal);
initialVal = 2.5;
console.log(typeof initialVal);
initialVal = undefined;
console.log(typeof initialVal);
initialVal = null;
console.log(typeof initialVal);
/******************* dynamic typing - Ends ***********************/

/******************* Reference Types - Starts ***********************/
//1. object 2. array 3. function
// object = dictionary
let personObj1 = {
    fullName: "Ram",
    age: 25
};
let personObj = {
    fullName: "Ram",
    age: 25
};
console.log(personObj1);
personObj1.fullName = "Krishna";
console.log(personObj1);
personObj1["fullName"] = "Shiva";
console.log(personObj1);
let selection = "fullName";
console.log(personObj1[selection]);
/******************* Reference Types - Ends ***********************/


/******************* Arrays - Starts ***********************/
let selectedColours = ["red", "blue"];
let selectedColours1 = ["red", "blue"];
console.log(selectedColours1);
console.log(selectedColours1[1]);
selectedColours1[2] = "green";
console.log(selectedColours1);
console.log(typeof selectedColours);
selectedColours1[3] = 3.0;
console.log(selectedColours1);
console.log(typeof selectedColours);
console.log(selectedColours1.length);
/******************* Arrays - Ends ***********************/

/******************* Function - Starts ***********************/
function greet() {
    console.log("Hello World");
}

function greetUser(userName) {
    console.log("Hello " + userName);
}

function greetUserFullName(firstName, lastName) {
    console.log("Hello " + firstName + " " + lastName);
}

greet();
greetUser("John");
greetUser("Mary");
greetUserFullName("Will", "Smith");
greetUserFullName("Jaden");

function square(number) {
    return number * number;
}
let input_no = 5;
let result = square(input_no);
console.log(result);
console.log(square(5));
console.log(typeof square)
/******************* Function - Ends ***********************/


// https://www.youtube.com/watch?v=W6NZfCO5SIk


/**************** let vs var - Starts ***********************/

function exampleScope() {
    if (true) {
        // console.log(x);
        console.log(y);
        let x = 10;
        var y = 20;
        console.log(x); // Output: 10
        console.log(y); // Output: 20
    }
  
    // x is not accessible here (ReferenceError)
    // console.log(x)
    console.log(y); // Output: 20 (y is accessible here)
  }
  
exampleScope();

/********************** let vs var - Ends *************************/

/******************* const - Starts ***********************/
varName = "CSS";

console.log(varName);

const PI = 3.14159;
console.log(PI); // Output: 3.14159

// Attempt to reassign a const variable will result in an error:
// PI = 3.14; // TypeError: Assignment to a constant variable.

// Example with object
const person = { name: "John", age: 30 };
console.log(person); // Output: { name: 'John', age: 30 }

// It's possible to modify the object properties
person.age = 31;
console.log(person); // Output: { name: 'John', age: 31 }

// But reassigning the variable itself is not allowed
// person = { name: "Alice", age: 25 }; // TypeError: Assignment to a constant variable.

/******************* const - Ends ***********************/